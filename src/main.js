import Vue from "vue";
import { registerSW } from "virtual:pwa-register";
//import "boxicons";
import vuesax from "vuesax";
import "vuesax/dist/vuesax.css";
import "./assets/all.css";
import router from "./router";
import App from "./App.vue";
const updateSW = registerSW({
  onNeedRefresh() {},
  onOfflineReady() {},
});

Vue.use(vuesax);
new Vue({
  router,
  render: (h) => h(App),
}).$mount("#app");
